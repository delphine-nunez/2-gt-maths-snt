# SNT 
## Prise en main de l'IPAD

[prise en main](https://ec-morlaix.github.io/info/priseenmain/)

## Introduction


> une petite histoire de l'informatique : [vidéo](https://youtu.be/16udHcMYRFA)


!!! note " A faire"  
=== "pendant la lecture de la vidéo"
    - prendre des notes pour repérer les mots clés, noms des personnes cités ...  
    - reprendre quelques passages de la vidéo pour compléter vos notes  
=== " recherches"
    - Effectuer des recherches sur les personnes cités 
    - Comprendre les notions abordées dans la vidéo.
=== " Mise au propre"
    - Effectuer un document numérique :  
        - Support au choix (Word, Diaporama, Canva...)
        - Chronologie de la recherche informatique
        - Biographie détaillée d'une des personnes citées (au choix)
        - Ce travail peut être fait par 2.



## Présentation

La SNT se découpe en 7 thèmes liés à l'informatiques :

![SNT](./snt.png)

Le thème des objets connectés sera abordé en AP-SI.
## Fil Rouge 1 : Internet  

Le thème internet sera étudié tout au long de l'année car il impacte l'ensemble des autres thèmes.

## Fil Rouge 2 : Python  

Python est également étudié tout au long de l'année car certains thèmes nécessitent de la programmation. De plus, vous aurez besoin des connaissances en python pour les enseignements scientifiques (Maths, Physique, SI, SVT...)

[FuturCoder](https://fr.futurecoder.io/course/#IntroducingTheShell)  
[Capytale](https://capytale2.ac-paris.fr/web/c-auth/login)  

## Thème 1 : Le Web 
[Web](./Web.md)

## Thème 2 : Les Réseaux Sociaux
[Réseaux Sociaux](./RS.md)
<!--
## Thème 3 : Les Structures de données
[Structures Données](./SD.md)

## Thème 4 : La photographie numérique
[La photographie numérique](./PN.md)

## L'intelligence artificielle
[Intelligence artificielle](./IA.md)


## Fil Rouge 1 : Internet
Ce thème est présent dans beaucoup de sujet que nous verrons cette année. Donc, au lieu de le traiter comme les autres thèmes, nous l'aborderons plus dans un esprit pratique, notamment dans le Web, les Réseaux Sociaux et les Structures de données. 

-->

