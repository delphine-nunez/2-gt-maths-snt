# Thème 1 : Le web 

## Vidéos

1. Regarder chacun une des vidéos suivantes et relever les mots clefs et acronymes associés au thème du web.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/GqD6AiaRo3U" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/RHljpE7pZh8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/bD6oideRbg8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/68TNDVJKjp0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

2. Mise en commun des notes pour construire un cours sur les notions du thème


## Introduction aux langages du Web : HTML/CSS


## Publier un site Web :

Les activités pour créer les pages Web seront à faire sur le site [Capytale](https://capytale2.ac-paris.fr/web/c-auth/login)

## Sujets à aborder en projet Web

Par équipe vous allez publier une page web contenant au moins un titre et deux sous-titres, trois paragraphes, une images, un lien hypertexte afin de présenter l'un des sujets suivants en répondant à minima aux questions associées :

### Le HTML

- Donner la signification de HTML ?
- Quel est l'histoire de ce langage ?
- Est-ce un langage de programmation ou de description ?
- A quoi sert-il ?
- Quels sont les marqueurs de ce langage ?
- Lister les principales balises, à quoi servent-elles ?
- Aujourd'hui, l'usage de quelle version de HTML est recommandée par le W3C ?
- ...


### Le CSS 

- Donner la signification de CSS ?
- Quel est l'histoire de ce langage ?
- Est-ce un langage de programmation ou de description ?
- A quoi sert-il ?
- Quels sont les selecteurs de ce langage ?
- Quelle est la structure de son code ?
- Aujourd'hui, l'usage de quelle version de CSS est recommandée par le W3C ?
- ...

### Les URL

- Dans la barre d'adresse de votre navigateur web vous trouverez, entrer le site  : "https://www1.ac-grenoble.fr/article/formations-scolaires-proposees-121615". 
- Expliquer chacune des parties.
- A l'aide du schéma suivant, définissez et donnez des exemples d'URL
![URL](./Images/web-arborescence.png)<br>



### Les cookies

Une petite vidéo :

<div style="position:relative;padding-bottom:56.25%;height:0;overflow:hidden;"> <iframe style="width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden" frameborder="0" type="text/html" src="https://www.dailymotion.com/embed/video/x16lt53" width="100%" height="100%" allowfullscreen > </iframe> </div>

### RGPD

1. Quelle est la signification du sigle RGPD ?
2. Lister quelques exemples d'utilisation.
3. Quelles sont les conséquences sur internet ?


### Les clients / serveurs
*Ce sujet peut demander quelques notions sur Internet que vous pourrez définir et expliquer.*
 A l'aide du site [Client-serveurs](https://pixees.fr/informatiquelycee/n_site/snt_web_clsv.html) vous pourrez :
 
 - Expliquer ce qu'est un réseau 
 - Définir dans ce contexte, les notions de clients et de serveurs
 - Donner les avantages et les inconvenients de ce type de réseau. 
 - Donner d'autres exemples de réseau, en les expliquant et donnant des applications. 

![Serveurs](./Images/baie-serveur.jpg)<br>

### Web Statique

1. Donner un exemple de page web statique
2. Quelle est l'utilité d'un web statique par rapport au dynamique ?
3. Quel langages permettent de rendre une page web statique interactive ?


### Web dynamique

1. Donne un exemple de page web dynamique
3. Quelle est l'utilité d'un web dynamique par rapport au statique ?
4. Quel langages permettent de rendre la page web dynamique ? 



### Le protocole HTTP

Voici un exemple de requête HTTP :

![http](./Images/web-requete-serveur.png)<br>

Réponse du serveur à une requête HTTP

Une fois la requête reçue, le serveur va renvoyer une réponse, voici un exemple de réponse du serveur :

![http](./Images/web-reponse-requete.png)<br>


Autre exemple de requête <br>
![http](./Images/web-http-questions.gif)

A l'aide de ces exemples et de recherche sur Internet :

- Expliquer dans les grandes lignes les requêtes HTTP
- Expliquer les images données en exemple en faisant une description : 
    - des URL
    - des fichiers attendus
    - des navigateurs web
    - du système d'exploitation
    - des requetes 
    - des réponses aux requêtes, proposez-en d'autres 
    - ...


### Les arnaques sur Internet


