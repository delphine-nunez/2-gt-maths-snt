# --------- PYODIDE:code --------- #
from math import *

def ir(a,b):
    # a2 désigne le carré de a, b2, le carré de b et d_ab le double produit.
    a2= ...
    b2= ...
    d_ab= ...
    print("le carré de a est ",...)
    print("le carré de b est ",...)
    print("le double produit est ",...)

print("On souhaite développer (a+b)^2 ou (a-b)^2")



# --------- PYODIDE:corr --------- #
from math import *

def ir(a,b):
    a2=a**2
    b2=b**2
    d_ab=2*a*b
    print("le carré de a est ",a2)
    print("le carré de b est ",b2)
    print("le double produit est ",d_ab)

print("On souhaite développer (a+b)^2 ou (a-b)^2")




# --------- PYODIDE:tests --------- #

print("calcul 1")
ir(8,11)
print("calcul 2")
ir(5,-9)
print("calcul 3")
ir(-78,56)
print("calcul 4")
ir(53,-14)


# --------- PYODIDE:secrets --------- #

# Pas de secrets
