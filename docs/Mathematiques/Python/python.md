---
author: Delphine NUNEZ 
title: Exercices python
tags:
  - Géométrie 
  - calculs
  - algèbre
---
# Chap 6 - Calcul littéral
???+ question "Exercice 1"
    Compléter la fonction et le programme permettant de calculer $a^2$, $b^2$ et $2ab$
    {{ IDE('ex1-6') }}

# Chap 3 - Configuration dans le plan
???+ question "Exercice 1"
    Ecrire une fonction qui calcule les coordonnées du milieu de deux points 
    
   {{ IDE('ex1') }}


???+ question "Exercice 2"
        Ecrire une fonction qui calcule la distance entre deux points 

   {{ IDE('ex2') }}
    