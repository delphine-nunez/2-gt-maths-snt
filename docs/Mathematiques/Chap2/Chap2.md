# Généralités sur les fonctions
!!! question "Objectifs"
    - Calculer une image ou un antécédent  
    - Construire et lire un tableau de valeurs  
    - Tracer la représentation graphique d’une fonction  
    - Lire graphiquement une image ou un antécédent  
    - Résoudre algébriquement (par le calcul) des équations  de type $f(x) = k$ et $f(x)=g(x)$  
    - Utiliser la calculatrice pour tracer la représentation d’une fonction, obtenir un tableau de valeur.  
    - Problèmes 
!!! info "Cours"
    [Chap2](./cours_chap2.pdf){:target="_blank"}    
!!! danger "A retenir"
    Soit $\mathcal{D}$ un ensemble de nombres réels. Définir une *fonction* $f$ sur $\mathcal{D}$ revient à associer, à chaque réel $x$ de $\mathcal{D}$, un réel **et un seul**, appelé *image* de $x$.  
    Une fonction peut être définie  
    - par une expression algébrique, par exemple : $f(x)=3x^2-5$  
    - par un graphe ![graphe](./graphe.png)  
    - par un tableau de valeur  ![table](./table.png)  
    - par un algorithme  ![algo](./algo.png)  
    
    $\mathcal{D}$ est *l'ensemble de définition de $f$*, c'est l'ensemble des valeurs de $x$ ppur lesquelles la fonction existe. $\mathcal{D}$ peut être l'ensemble des nombres réels $\mathbb{R}$, ou être constitué d'un ou plusieurs intervalles de $\mathbb{R}$.


!!! note "Image et antécédent" 
    !!! danger "A retenir"
        Soit $f$ une fonction définie sur un intervalle I.   
         Les phrases suivantes sont synonymes :
         >   - $y=f(x)$  
            - $y$ est l'image de $x$ par la fonction $f$.  
            - $x$ est un antécédent de $y$ par la fonction $f$.  
            - Le point de coordonnées $\left(x; y\right)$ est sur la courbe de $f$.   


         Les phrases suivantes sont synonymes.  
         >  - Déterminer l'image de $3$ par $f$.  
            - Calculer $f(3)$.  
            - Déterminer l'ordonnée du point de la courbe de $f$ d'abscisse $3$.  


         Les phrases suivantes sont synonymes :  
         >  - Déterminer les antécédents de $4$ par $f$.  
            - Résoudre $f(x)=4$.  
            - Déterminer les abscisses des points de la courbe de $f$ d'ordonnée $4$.  

    === " Activité "
        [Exemples](./exemple-fonctions.pdf){:target="_blank"}   

    === " Exemples résolus"
        Application et méthodes p42  
        Application et méthodes p44  
        Application et méthodes p46  

    === "Exercices"
        N°2-3p38  
        N°27-28p54  
        N°30-31-32p54-55  
   
   
!!! note "Parité d'une fonction"
    !!! danger " A retenir"
        Soit $f$ une fonction définie sur un intervalle I.  
        > On dit que $f$ est paire si pour tout $x, f(-x)=f(x)$  
        > On dit que $f$ est impaire si pour tout $x, f(-x)=-f(x)$  

    === " Activité "
        [Fiche](./exemple-parite.pdf){:target="_blank"}   
    === "Exemples résolus"
        Application et méthodes p45  
    === "Exercices "  
        N°46p58  

!!! note "Résolutions graphiques d'équations"
    === " Equation de type $f(x)=k$"
        Soit $f$ une fonction définie sur un intervalle I. et $k$ un réel.  
        Résoudre graphiquement l'équation $f(x)=k$ revient à chercher les abscisses des points d'intersection de $\mathcal{C}_f$ avec la droite d'équation $y=k$
    === " Equation de type $f(x)=g(x)$"
        Soit $f$ et $g$ deux fonctions définies sur un même intervalle I.  
        Résoudre graphiquement l'équation $f(x)=g(x)$ revient à chercher les abscisses des points d'intersection de $\mathcal{C}_f$ et $\mathcal{C}_g$.
    === " Exemple "
        [Fiche](./exemple-equation1.pdf){:target="_blank"}   
    === "Exercices"
        N°50-51 p59
        N°52 p60  
        N° 29p54  
!!! failure "Travail différentié"
    [Niveau 1](./FonctionNiv1.pdf){:target="_blank"}  
    [Niveau 2](./FonctionNiv2.pdf){:target="_blank"}  
    [Niveau 3](./FonctionNiv3.pdf){:target="_blank"}  

!!! note "Variations de fonctions - Extremums"
    === " Activités"
        [Fiche](./activiteVariations.pdf){:target="_blank"}    
    === " Cours"
        [Variations](./cours-variations.pdf){:target="_blank"}  
        [Extremum-Comparaison](./cours-extremum.pdf){:target="_blank"}  
    === "Exercices"
        Tableau de variations - Extremums
        N°27p80  
        N°28-29p81  
        Comparaison d'images  
        [Exercices](./exercices-variations.pdf){:target="_blank"}  
        N°23p80  
        N°30p81  


