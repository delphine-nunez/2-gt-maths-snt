# Mathématiques

## Manuel numérique

> [Livre](https://www.calameo.com/read/0005967290f026f1d6ada){target="_blank"}  

Des activités ou ressources ont été inspirées par le site de Louis Paternault.
## Chapitre 1  
[Ensembles de nombres](./Chap1/Chap1.md)  

## Chapitre 2  
[Généralités sur les fonctions](./Chap2/Chap2.md)  

## Chapitre 3  
[Configuration du plan](./Chap3/Chap3.md) 


## Chapitre 4  
[Fonctions affines](./Chap4/Chap4.md) 


## Chapitre 5  
[Notions de vecteurs](./Chap5/Chap5.md) 

## Chapitre 6
[Calcul littéral](./Chap6/Chap6.md)