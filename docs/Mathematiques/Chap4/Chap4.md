# Fonctions affines 

!!! question "Objectifs"
    - Définitions : Fonction affine, fonction linéaire  
    - Définitions : Coefficient directeur, ordonnée à l'origine  
    - Compétences :  
        * Déterminer l’équation d’une fonction affine définie par les images de deux nombres distincts;    
        * Déterminer les variations d’une fonction affine ;    
        * Calcul littéral : résoudre des équations, des inéquations ;   
        * Déterminer le signe d’une fonction affine ;   
        * Représenter le résultat sous la forme d’un tableau de signes ;  
        * Lire un tableau de signes
!!! info "Cours"
    [Chap4](./Cours.pdf){:target="_blank"}  
    [Vidéo - tracer une représentation graphique 1](https://youtu.be/f02v-bKuZj4?si=HgCVDPkD3vZbDvHa){:target="_blank"}   
    [Vidéo - Partie 2](https://youtu.be/LiBfYGiNlKA?si=3s0ryPVt4lcN6E4j){:target="_blank"}   
    [Vidéo - Trouver f à partir de la représentation graphique](https://youtu.be/yjXaBozzOyI?si=mBOcXUc-WdN7BV5y){:target="_blank"}  
    [Vidéo - Trouver f par le calcul](https://youtu.be/dh9lXL2ZdG4?si=c_5ofnjVxdOl7N91){:target="_blank"}  
    [Vidéo - Tableau de signes 1 ](https://youtu.be/4L-bJpIYAPU?si=zMUmUSDPfrlfdCax){:target="_blank"}  
    [Vidéo - Tableau de signes 2](https://youtu.be/LAKz20vBgZw?si=kYPbU6FIRd0xJ0KA){:target="_blank"}  

!!! note "Définitions"
    === " Cours"
        !!! danger "A retenir"
            **Définition :**  
            Une _fonction affine_  $f$ est une fonction définie sur $\mathbb{R}$ par $f(x)=ax+b$ avec $a,b$ deux nombres réels.  
            $a$ est appelé **coefficient directeur**  
            $b$ est appelé **ordonnée à l'origine**  
            Remarque : $b=f(0)$, $b$ est l'image de 0 par la fonction $f$.  
            Dans un repère (O,i,j), la **représentation graphique** d'une fonction affine est une droite non parallèle à l'axe des ordonnées.  
            **Théorème :**  
            Soient deux points $A(x_A,y_A)$ et $B(x_B,y_B)$ le coefficient directeur peut être calculé par la formule :  
            $a=\dfrac{y_B-y_A}{x_B-x_A}$  
            l'ordonnées à l'origine se déduit par l'un des calculs $b=y_A-a.x_A$ ou $b=y_B-a.x_B$.  
    
    === "Exemples"  
        **Exemple 1** :  
        Déterminer l'expression des fonctions affines dont la représentation est donnée ci-dessous.  
        ![graphe](./Lecture-f-affine.PNG)  
        **f** (représentée par la droite verte) :  
        $b=f(0)=2$  
        $a=\frac{3}{1}$  
        Donc $f(x)=3x+2$  
        **g** (représentée par la droite rouge) :  
        $b=g(0)=-5$  
        $a=\frac{-2}{1}$  
        Donc $g(x)=-2x-5$    
        **h** (représentée par la droite bleue) :  
        $b=h(0)=-1$  
        $a=\frac{2}{3}$  
        Donc $h(x)=\frac{2}{3}x-1$  


        **Exemple 2** :  
        Soit $f$ la fonction affine telle que $f(2) = 3$ et $f(6) = 1$.  
        - Déterminer les coordonées de deux points de la représentation graphique de $f$.  
        - Déterminons l'expression de $f$.  
        - $A(2;3)$ et $B(6,1)$ sont deux points de la représentation graphique de $f$. Celle-ci est une droite.  
        - $a=\frac{y_B-y_A}{x_B-x_A}=\frac{1-3}{6-2}=\frac{-2}{-4}=-\frac{1}{2}$  
        Donc $f(x)=-\frac{1}{2}x+b$.
        Pour chercher $b$, on prend une donnée de l'énoncé (soit l'image de 2, soit l'image de 6, le résultat sera le même.)
        $f(2)=-\frac{1}{2}\times 2+b=-1+b=3 \Leftrightarrow b=3+1=4.$ 
        Donc $f(x)=-\frac{1}{2}x+4$  



    === "Exercices"
        !!! info "Vocabulaire"
            **Niveau 1** : N°17-18p105, N°39p107  
            **BILAN**: devoirs kwyk   
            **Niveau 2** : N°37p107  - N°43-44p107.  
            **Niveau 3** : N°33-34p106  

        !!! info "Méthodes"  
            [Méthodes - Trouver l'expression de $f$](./trouver-f.pdf){:target="_blank"}  
            [Méthodes - Tracer la représentation graphique de $f$](./tracer-Cf.pdf){:target="_blank"}  

        !!! info "Propriétés"
            **Niveau 1** : [exo](./ex_pte.PNG){:target="_blank"}   
            **Niveau 1** : N°23p105 - N°48-49p108   
            **Niveau 1** :[exo2](./ex2_pte.PNG){:target="_blank"}  

!!! note "Sens de variation"
    === "Activité"
        [Variations](./activite-variations.pdf){:target="_blank"}  
    === "Cours" 
        !!! alerte "A retenir"
            **Propriété** : $a$ le coefficient directeur de la fonction affine $f$.  
            - Si $a>0$ alors $f$ est croissante.  
            - Si $a<0$ alors $f$ est décroissante.             
    === "Exercices"
        N°25p105  

!!! note "Equations - Inéquations"
    === "Exercices Equations"
        Résoudre les équations suivantes :  
        1. $3x+4=2x+9$  
        2. $3x+1=7x+5$  
        3. $5-4x=0$  
        4. $2x +1− (2 +x)− 7= 3x +7$  
        5. $2(x− 1)− 3(x +1) = 4(x− 2)$  
        6. $13x +2− (x− 3) = x− 5− 3(x +12)+4x$  
        7. $\frac{3}{2}x +4= 2x− 5$  
        8. $\frac{x-1}{4}-5=\frac{2x-3}{2}+\frac{3}{4}$.

    === "Exercices Inéquations"
        [Méthodes - Résolutions d'inéquations](./inequations.pdf){:target="_blank"}  
    === "Applications aux fonctions affines"
        N°30 p106  
        N°50 p108  
        N°56 p109  
        
!!! note "Tableaux de signes"
    === "Cours" 
        !!! danger "A retenir" 
            Le but de cette partie est de déterminer le signe d'une fonction affine $f$. C'est à dire que l'on veut savoir sur quel intervalle $f(x)>0$ et sur quel intervalle $f(x)<0$. 
            Le signe positif ou négatif de $f$ se symbolise dans un tableau de signe.  
            ![Tableau](./signe.jpg)  
            Pour cela, nous allons montrer deux méthodes.  
            === "Méthode 1 : par inéquation"
                En résolvant par le calcul $f(x)>0 \Leftrightarrow ax+b>0 \Leftrightarrow ax>-b$.  
                - Si a>0, cela donne $x>\dfrac{-b}{a}$. Donc $f(x)>0$ sur $]\dfrac{-b}{a} ;+\infty[$  
                - Si a<0, cela change l'ordre et donne $x<\dfrac{-b}{a}$. Donc $f(x)>0$ sur $]-\infty;\dfrac{-b}{a}[$   
                On procède de la même façon pour trouver l'intervalle sur lequel $f(x)<0$ (le calcul est strictement le même.)  
            === "Méthode 2 : à l'aide des variations de $f$"
                ![Tableau](./tab-signes.png)   
                En fonction des variations de la fonction $f$, on peut en déduire le signe grâce au graphique ci-dessus. Il reste à résoudre $f(x)=0$ pour trouver $\dfrac{-b}{a}$.  

    === "Exercices"
        N°26 p105  
        N°65 p110  
        N°69p110

!!! note "Exercices"
    [exo](./exercice.jpg){:target="_blank"}  
    [en Autonomie](./Exercices.pdf){:target="_blank"}  


