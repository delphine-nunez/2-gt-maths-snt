# Notion de vecteurs

!!! question "Objectifs"
    Déterminer l’image d’un point par une translation   
    Utiliser le lien entre égalité de vecteurs et parallélogramme  
    Utiliser le lien entre vecteurs et milieu d’un segment  
    Utiliser le lien entre vecteurs et symétrie  
    Manipuler la somme de vecteurs  
    Manipuler la relation de Chasles  
    Les coordonnées de vecteurs 
<!--
!!! info "Cours"
    [Chap5](./Cours-Chap5.pdf){:target="_blank"}  
    -->
!!! note "Définitions"
    === " Cours"
        !!! danger "A retenir"
            **Définition :**  
            Soient A et B deux points du plan.  
            On appelle _translation_ qui transforme A en B, la transformation qui à tout point C du plan associe l'unique point D ABDC est un parallélogramme.  
            ![vecteur](./vecteurs.jpg)  
            A cette translation, on associe le _vecteur_$\overrightarrow{AB}$.  
            Le point A est appelé _origine_ du vecteur $\overrightarrow{AB}$, le point B est son _extrémité_.  
            On dit que B est _l'image_ de A par la translation de vecteur $\overrightarrow{AB}$.    
            **Cas particulier** : 
            - Si A et B sont confondus, la translation de vecteur $\overrightarrow{AB}$ laisse invariant tous les points du plan. Le vecteur $\overrightarrow{AA}=\vec{0}$ est appelé _vecteur nul_.  
            - A est l'image de B par la translation de vecteur  $\overrightarrow{BA}$. Le vecteur  $\overrightarrow{BA}=- \overrightarrow{AB}$ est le vecteur opposé à  $\overrightarrow{AB}$.  
            **Propriété** Soient A et B deux points du plan, M, N et O trois points ayant pour image M',N' et O' par la translation de vecteur $\overrightarrow{AB}$. Alors :
            - Si M, N et 0 sont alignés alors M', N' et O' sont alignés.  
            - L'image du segment [MN] est le segment [M'N'].  
            - Si O est le milieu de [MN], alors O' est le milieu de [M'N'].
            **Bilan** : La translation conserve les angles et les distances.  
        !!! danger "A retenir"
            **Définition** Pour désigner l'unique vecteur associé à la translation qui transforme A en B et C en D, on peut utiliser les lettres en écrivant $\overrightarrow{AB}=\overrightarrow{CD}=\vec{u}$.  
            **Remarque** Un vecteur admet un infinité de représentants.   
            **Propriété** Un vecteur $\overrightarrow{AB}$ est déterminé par :  
            - Sa direction (la droite (AB)),  
            - Son sens (de A vers B),  
            - Sa norme (La longueur AB), noté $\left\lVert\overrightarrow{AB}\right\rVert$  
            **Propriété** Deux vecteurs $\overrightarrow{AB}$ et $\overrightarrow{CD}$ sont égaux si et seulement si ABDC est un parallélogramme (Attention à l'ordre des sommets)  
            **Propriété** Soient A, B et I trois points du plan. Les propositions suivantes sont équivalentes :  
            - I est le milieu de [AB],  
            - $\overrightarrow{AI}=\overrightarrow{IB}$  
            - $\overrightarrow{AB}=2.\overrightarrow{AI}$ 


    === "Exemples"  
        [Activité](./activite-intro.pdf){:target="_blank"}  
    === "Exercices"
        N°15p183  
        N°28-29p184  
        N°31-33 p185  
        N°36-38p186  

!!! note " Somme de vecteurs"
    === "Cours"
        !!! danger "A retenir"
            **Propriété** Relation de Chasles : Pour tous points A,B, C du plan, on a   
            $\overrightarrow{AB}+\overrightarrow{BC}=\overrightarrow{AC}$  
            ![Chasles](./Chasles.jpg)  
            **Propriété** Règle du parallélogramme :    
            $\overrightarrow{AB}+\overrightarrow{AC}=\overrightarrow{AD}$ si et seulement si ABDC est un parallélogramme.  
            ![parall](./somme-par.jpg)  
    === "Exercices"
        N°16p183  
        N°39-41p186  
        N°43-46p187  

!!! note "Vecteurs dans un repère orthonormé"
    === "Cours"
        !!! danger "A retenir"
            **Définition** Le plan muni d'un repère (O,I,J) quelconque. Pour tout vecteur $\vec{u}$ du plan, il existe un unique point M(x,y) tel que $\vec{u}=\overrightarrow{OM}$. Alors :  
            $\vec{u} \begin{pmatrix} 
            x\\ 
            y 
            \end{pmatrix}$.  
            Le couple $(x,y)$ est applelé _coordonnées_ de $\vec{u}$.  
            **Propriété** : Soient $A(x_A,y_A)$ et $B(x_B,y_B)$ deux points du plan. Alors le vecteur $\overrightarrow{AB}$ a pour coordonnées  $\begin{pmatrix} 
            x_B-x_A\\ 
            y_B-y_A 
            \end{pmatrix}$.  
            **Propriété** : Somme de vecteurs :  
            Soient $\vec{u} \begin{pmatrix}
            x\\
            y
            \end{pmatrix}$ et $\vec{v} \begin{pmatrix}
            x'\\
            y'
            \end{pmatrix}$
            alors la somme $\vec{u}+\vec{v} \begin{pmatrix}
            x+x'\\
            y+y'
            \end{pmatrix}$
    === "Exemples"
        **Ex 1**  
        Donner les coordonnées des vecteurs $\vec{u}$ et $\vec{v}$
        ![ex](./coordonnees_vec.jpg)   
        **Ex 2** Dans un repère, tracer les vecteurs $\vec{a}\begin{pmatrix} 
        2\\ 
        -1 
        \end{pmatrix}$ et  $\vec{b} \begin{pmatrix} 
        0\\ 
        1 
        \end{pmatrix}$  
        **Ex3** Dans un repère, on donne les points A(1,1) ; B(3,2) ;C(2,0) et D(-3,-1). Déterminer dans les coordonnées des vecteurs $\overrightarrow{AB}$ et $\overrightarrow{CD}$.
    === "Exercices"
        N°19-20-22p183
        N°50-53-54p188  
        N°56-58-59-61p189



!!! note "Exercices corrigés bilan L.PATTERNAULT"  
    [Parallelogramme](./exo-parallelogramme.pdf)   
    [Exo-bilan-sans-coo](./exo-bilan-sans-coordonnees.pdf)  
    [Exo-bilan-avec-coo](./exo-bilan-avec-coordonnees.pdf)  



