# Calcul littéral

!!! note "objectifs"
    - Connaitre les méthodes de factorisation et de développement.  
    - Connaitre les identités remarquables :
      $(a+b)^2=a^2+2ab+b^2$.  
      $(a-b)^2=a^2-2ab+b^2$.  
      $(a-b)(a+b)=a^2-b^2$.  
      Et savoir les utiliser et les appliquer.  
    - Savoir résoudre des équations produit nul.


!!! note "dominos"
    [KillianA](Dominos/Killian_A%202025-02-07%2013_48_58.jpeg){:target="_blank"}    
    [KillianB](Dominos/Killian_B%202025-02-07%2013_46_42.jpeg){:target="_blank"}    
    [LorisA](Dominos/Loris_A%202025-02-07%2013_46_59.jpeg){:target="_blank"}    
    [LorisB](Dominos/Loris_B%202025-02-07%2013_46_22.jpeg){:target="_blank"}    
    [AnaisG](Dominos/AnaisG_A%202025-02-07%2013_47_35.jpeg){:target="_blank"}    
    [AdeleD](Dominos/AdeleD_B%202025-02-07%2013_47_55.jpeg){:target="_blank"}    
    [Yuna](Dominos/Yuna_B%202025-02-07%2013_48_40.jpeg){:target="_blank"}    

!!! note "Factorisation - Développement"
    !!! info "Généralités"
        === "Cours"
            !!! danger "A retenir"
                === "somme ou produit"
                     Donner des exemples d'expressions littérales  
                     - somme ou différence de termes  
                     - produit de facteurs  
                === "Définitions"
                     **Développer** c'est transformer un produit en somme  
                     **Factoriser** c'est transformer une somme en produit  
                === "Exemples" 
                    - $x(4+y)=4x+xy$ est le développement transformant le produit des facteurs $x$ et $4+y$ en somme.  
                    - $6x+30=6(x+5)$ est la factorisation transformant la somme des termes $6x$ et $30$ en produit. 
                === "Formules" 
                    **Distributivité** $k(a+b)=k\times a +k \times b$  
                    **Double distributivité** $(a+b)(c+d)=ac+ad+bc+bd$
        === "Exercices"
            1. Développer les expressions suivantes :  
            $A= 4(5+x)$  
            $B=(4x+6)\times 3$  
            $C=-6(-2x+4)$  
            $D=-(5-x)$  
            $E=(2x+3)(x+8)$  
            $F=(-3+x)(4-5x)$  
            $G=2(3+x)(3-2x)$  
            $H=2x(1-x)-(x-3)(3x+2)$  
            2. Factoriser les expressions suivantes :  
            **Niveau 1**
            $A=4x-4y+8$  
            $B=3t+9u+3$  
            $C= 4t-5tx+3t$  
            $D= x^3+3x^2-5x$  
            $E=3x^2-x$  
            **Niveau 2**
            $A=3(2+3x)-(5+2x)(2+3x)$  
            $B=(2-5x)^2-(2-5x)(1+x)$  
            $C=5(1-2x)-(4+3x)(2x-1)$  
    !!! info "Identités remarquables"
        === "Formules"
            !!! danger "A retenir"
                $(a+b)^2=a^2+2ab+b^2$.  
                $(a-b)^2=a^2-2ab+b^2$.  
                $(a-b)(a+b)=a^2-b^2$.  
            !!! note "Exemples"
                $(x+3)^2 = x^2 +2\times x\times 3+3^2 =x^2 +6x+9$  
                $(x-5)^2=x^2 -2\times x\times 5+5^2 =x^2 -10x+25$  
                $(2x-1)(2x+1)=(2x)^2-1^2=4x^2 -1$
        === "Démonstration"
            **Formule 1** :  
            ![IR1](./IR1.jpg){width=25%}  
            1. Ecrire les aires de tous les quadrilatères contenus dans le grand carré.  
            2. Donner la longueur du coté du grand carré et en déduire son aire.  
            3. En déduire le développement de $(a+b)^2$  
            **Formule 2** :  
            ![IR2](./IR2.jpg){width=25%}  
            1. Ecrire les longueurs manquantes et en déduire l'aire du grand carré et de tous les quadrilatères.  
            2. En isolant $(a-b)^2$ en déduire son développement.  
            **Formule 3** :  
            ![IR3](./IR3.jpg){width=25%}  
            1. Ecrire la longueur manquante et en déduire l'aire du grand carré et de tous les quadrilatères.  
            2. En isolant $a^2-b^2$ en déduire une factorisation.   
        ===  "Exercices " 
            1. Développer les expressions suivantes :    
             **Niveau 1**   
             $A=(x+5)^2$  
             $B=(4x-2)^2$   
             $C=(2x+3)^2$  
             $D=(6x-7)(6x+7)$  
             **Niveau 2**   
            $A=(3x-2)^2+(x-4)(2-x)$  
            $B=(2x-3)(2x+3)-(7x+2)(3x+1)$  
            $C=2(x+4)+(4x-5)^2$  
            2. Factoriser les expressions suivantes :  
             **Niveau 1**   
            $A=x^2+2x+1$  
            $B=25x^2-4$  
            $C=16x^2-40x+25$  
            $D=9x^2-36$  
            **Niveau 2**  
            $A=(2x+3)^2-49$  
            $B=1-(2-5x)^2$  
            **Niveau 3**  
            $A=(3x+1)^2-(5x+2)^2$  
            $B=-(4x+3)^2+(7x-5)^2$
        === "Pour s'entrainer"
             [ActivitéMathsAléa](https://capytale2.ac-paris.fr/web/c/eb75-5711747){:target="_blank"}    
        === "Pour aller plus loin"
            Développer : 
            $A= (3x+5y-3)^2$  
            $B=(2x-4)^3$ 
!!! note " Equations produit-nul"  
    === "Cours"
        !!! danger "A retenir"
            **Propriété** :  Si $a\times b =0$ alors $a=0$ ou $b=0$  
        !!! note "Exemple"
            $(2+5x)(3x-7)=0$  
            $(2x-5)^2=0$    
            $x^2-49=0$  
    === "Exercices"
        !!! info "Résolution d'équations"
            **Niveau 1**  
            Résoudre les équations suivantes :  
            $(4x+6)(3-7x)=0$  
            $x(4x+1)=0$  
            $(x-3)(x+3)=0$  
            $(1+x)(2x−3)(4−x)=0$  
            **Niveau 2**         
            Factoriser les expressions suivantes et résoudre les équations suivantes   
            $4x^2−3x=0$  
            $x^2-81=0$  
            $25x^2+30x+9=0$  
            $(4x-1)(2-3x)-(2-3x)(5x+1)=0$  
            $(2x+1)(2−7x)=(1+x)(2x+1)$  

!!! note "Tableaux de signes et inéquations"
    === "Méthode"
        !!! danger "Rappel"
            ![Tableau](./tab-signes.png)   
            En fonction des variations de la fonction $f$, on peut en déduire le signe grâce au graphique ci-dessus. Il reste à résoudre $f(x)=0$ pour trouver $\dfrac{-b}{a}$. 
        !!! danger "Application aux produits de facteurs"
            <iframe width="560" height="315" src="https://www.youtube.com/embed/50CByVTP4ig?si=l4XUcPs1lTRKqp3Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe> 
    === "Exercices"
    
!!! note "Résolutions de problèmes"