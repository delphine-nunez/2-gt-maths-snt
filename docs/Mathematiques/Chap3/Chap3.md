# Configuration du plan

!!! question "Objectifs"
    - Définitions : Repère, repère orthogonal, repère orthonormé,  
    - Définitions : Origine, abscisse, ordonnée,  
    - Formule du milieu,  
    - Formule de la longueur d'un segment,  
    - Propriétés des symétries centrales.
    - Propriétés des triangles, des parallélogrammes, du cercle,  
    - Tangente à un cercle,  


!!! info "Cours"
    [Chap3](./Cours_Chap3.pdf ){:target="_blank"}  

!!! note " Définitions - repérage"
    === "Cours"
        !!! danger "A retenir"
            - On appelle **repère (O;I,J)**, la donnée de 3 points non alignés du plan O, I, J.  
            - O est appelé **origine** du repère,   
            - (OI) est appelé **axe des abscisses**,  
            - (OJ) est appelé **axe des ordonnées**,  
            - Si OIJ est un triangle rectangle en O, le repère est dit **orthogonal**, si, de plus OI=OJ, le repère est dit **orthonormé**.  
            - Tout point M du plan est repéré par un unique couple $(x,y)$ appelé **coordonnées de M**, $x$ est **l'abscisse** de M, $y$ est **l'ordonnée** de M.  
            ![repere](./repere.png)  
    === "Exemple"
        ![exemple](./exemple-reperage.png)  
        1. Sur le cahier, placer les points en respectant les carreaux.  
        2. Pour chaque repère $(O;I,J),(A;B,C),(J;A,C)$, donner :  
        - La nature du repère;  
        - Les coordonnées de A et D.  
        3. Placer les points E, F et G :  
        - $E(1;3)$ dans le repère $(O;I,J)$  
        - $F(3;1)$ dans le repère $(A;B,C)$  
        - $G(1;2)$ dans le repère $(J;A,C)$
    === "Exercices"
        Activité Ap146  
        N°16-17p159  
        N°33p160  


!!! note " Coordonnées du Milieu et applications "
    === "Cours" 
        !!! danger " A retenir"
            Soient $A(x_A;y_A)$ et $B(x_B;y_B)$ deux points du plan muni d'un repère.  
            I est le **milieu** de [A,B] si et seulement si  
            $x_I=\dfrac{x_A+x_B}{2}$  
            et  
            $y_I=\dfrac{y_A+y_B}{2}$  
    === "Rappels et application à la géométrie" 
        C est le symétrique de A par rapport à B si et seulement si B est le milieu du segment [AC].  
        ![sym](./Symetrie.png)  
        Un quadrilatère est un parallélogramme si et seulement si ses diagonales ont le même milieu.  
        ![parall](./parall.png)  
    === "Exemple 1"
        Dans un repère, on considère deux points $A(2;7)$ et $B(-4;2)$. Déterminer les coordonnées du milieu $I$ de $[AB]$.  
        En appliquant la propriété, on obtient :  
        $x_I = \dfrac{x_A+x_B}{2} = \dfrac{2+(-4)}{2}=\dfrac{-2}{2}=-1$  
        $y_I = \dfrac{y_A+y_B}{2} = \dfrac{7+2}{2}=\dfrac{9}{2}$  
        Les coordonnées de $I$ sont donc $I(-1; 4,5)$. 
    === "Exemple 2"
        Dans le plan muni d'un repère quelconque, on considère le point $I$, milieu du segment $[AB]$. On connait les coordonnées $I(2,3; -9)$ et $B(5; -2)$. Quelles sont les coordonnées de $A$ ?  
        Considérons d'abord les abscisses.  
        Puisque $x_I=\dfrac{x_A+x_B}{2}$, alors $2,3=\dfrac{x_A+5}{2}$  
        $2\times2,3=x_A+5$  
        $4,6=x_A+5$  
        $4,6-5=x_A$  
        $-0,4=x_A.$  
        De même pour les ordonnées : $y_I=\dfrac{y_A+y_B}{2}$, donc $-9=\dfrac{y_A-2}{2}$, et :  
        $2\times\left( -9 \right) = y_A-2$     
        $-18= y_A-2$  
        $-18+2= y_A$  
        $- 16=y_A$  
        Les coordonnées de $A$ sont donc $A(-0,4;-16)$.
    === "Exercices"
        N°20-21-23 p159  
        N°36 p161  
        N°40-41p162   
        [Exercice 1](../Python/python.md)

!!! note " Longueur d'un segment "
    === "Cours" 
        !!! danger " A retenir"
            Soient $A(x_A;y_A)$ et $B(x_B;y_B)$ deux points du plan muni d'un **repère orthonormé**.  
            La distance entre les points A et B est donnée par :  
            $AB=\sqrt{(x_B-x_A)^2+(y_B-y_A)^2}$    
    === "Rappels quadrilatères"
        [Rappels](./cours-quadrilateres.pdf){:target="_blank"}  
    === "Rappels triangles"
        [Rappels](./cours-triangles.pdf){:target="_blank"}  
    === "Démonstration"
        [Activité-démonstration](./distance-demonstration.pdf){:target="_blank"}  
    === "Exemple"
        Dans un repère orthonormé, on considère deux points $A(2;-7)$ et $B(-4; 2)$. Calculer une valeur exacte, puis une valeur approchée au centième, de la longueur $AB$.  
        En appliquant la propriété, on obtient :
        $AB = \sqrt{\left( x_B-x_A \right)^2+\left( y_B-y_A \right)^2}$  
            $= \sqrt{\left( -4-2 \right)^2+\left( 2-\left( -7 \right) \right)^2}$  
            $= \sqrt{\left( -6 \right)^2+9^2}$  
            $= \sqrt{36+81}= \sqrt{117}$  
        La longueur $AB$ est donc $\sqrt{117}$.  
    === "Applications"
        N°24-26 p159  
        N°47-48 p163  
        N°54 p164
        N°77 p168  
        [Exercice 2](../Python/python.md)
    === "Médiatrices" 
        !!! danger "Rappels"
            * La médiatrice d'un segment [AB] est la droite qui coupe perpendiculairement ce segment en son milieu.  
            * I appartient à la médiatrice du segment [AB] si et seulement si IA=IB
        !!! note " Exercice"
            Dans le repère orthonormé (O;I,J) on considère les points suivants :  
            • A(6;0)  
            • B(0;4)  
            • C(1;-1)  
            1) Faire une figure  
            2) Prouver que le triangle ABC est rectangle  
            3) On appelle K le milieu du segment [AB].  
                a) Calculer les coordonnées de K.  
                b) Prouver que K appartient à la médiatrice du segment [OC].  


!!! note " Configurations "
    === "Tangente" 
        !!! danger " A retenir"
            Tangente à un cercle C en un point.  
             **Propriété** : La tangente à un cercle au point A est perpendiculaire au rayon [OA].  

            ![Tangente](./Tangente.png){ width=50% }  
    === "Exercices tangente"  
        [Exercices](./tangente.pdf){:target="_blank"}  
    === "Exercices Configurations" 
        N°67 p 166  
        N°71 p166  
        N°74 p 167  

!!! failure "Exercices MathAlea" 
    [Exercices](./Exercices.pdf){:target="_blank"}   
    **Révision collège** : Exercices 5-7-8-9  
    **Niveau 1** : Exercices 1-2-4  
    **Niveau 2** : Exercices 3-6  
    **Niveau 3** : N°60p165 - 80p168

    
   

