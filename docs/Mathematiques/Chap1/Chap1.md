# Ensembles de nombres - Calculs numériques

!!! note "Cours"
    [Cours Chap1](./Chap1-ensembles-calculs.pdf){:target="_blank"}   
    
!!! note "Ensembles de nombres" 
    === "Rappels"
        [Rappels de propriétés](./calculs.pdf){:target="_blank"}  
        Valeurs approchées p330  
        Fractions p331-332  
        Puissances p332  
        Pour ceux qui rencontrent des difficultés...des [exercices corrigés](./exercices.pdf){:target="_blank"} en plus
    === " Activité"
        Ensembles de nombres  
        {{multi_qcm(

["Ce segment mesure à peu près ... cm. Cocher les valeurs qui peuvent convenir.", 
["2", "3.2","$3\\sqrt{2}$" , "$\\frac{5\\sqrt{3}}{2}$", "Tous les nombres décimaux"], [1, 2, 5]],
["Il a coupé la tarte en ... morceaux. Cocher les valeurs qui peuvent convenir.",
["4", "8", "2.3", "$3\\sqrt{2}$", "Tous les nombres entiers"], [1,2, 5]],
["On a mangé ... pommes. Cocher les valeurs qui peuvent convenir.", 
["2","$2\\sqrt{5}$","1" , "-12", "Tous les nombres entiers"], [1, 3, 5]],
["La météo annonce ... degré demain. Cocher les valeurs qui peuvent convenir.", 
["2", "-7" , "$\\frac{5\\sqrt{3}}{2}$", "$\\frac{5}{2}$", "Tous les nombres relatifs"], [1, 2, 5]],

)}}

    === "Cours"
        Définitions des ensembles (voir le cours pour les definitions des ensembles de nombres).  
        Différence entre $\subset$ et $\in$ :  
            - Un nombre appartient à un ensemble (par exemple $-3 \in \mathbb{Z}$).  
            - Si un ensemble est inclus dans un autre ensemble on utilise le symbole $\subset$ (par exemple $\mathbb{Q} \subset \mathbb{R}$).  
        Demonstration : Montrer que $\frac{1}{3} \notin \mathbb{D}$  
        Chiffres significatifs

    === "Exercices"
        Calculs exercices : p330-331-332  
        Ensembles  : N°4,7,12,13 p15  

!!! note "Intervalles"
    === "Activité" 
        Droite des réels - Intervalles  
    === "Cours"
        Intervalles  
        Unions, Intersections d'ensembles  
        Encadrements  
    
    ===  "Exercices"
        N°14,15,20p17  
        Dans chaque cas, déterminer $I\cup J$ et $I\cap J$:  
        $I=]2;5]$ et $J=[−3;4[$  
        $I=]2;5]$ et $J=]−\infty;4]$    
        $I=]−\infty;4]$ et $J=[−5;+\infty[$  
        $I=]6;+\infty[$ et $J=[−4;+\infty[$  



!!! note "Calculs"
    === "Fractions "
        N°45-46-47 p20    
        [corrigé](./corr_45-46-47.pdf)
    === "Racines carrées" 
        N°34-35-36-37-38-39 p20  
        [corrigé](./corr34-39.pdf)
    === "Puissances"
        N°54-55 p20  
        [correction](./corr_54-55.pdf)

!!! note "Valeur absolue"
    === "Définition"
        Valeur absolue comme distance entre deux réels  
        $\lvert x-a\rvert <r \Leftrightarrow x\in ]-r+a,r+a[$  
        
    === "Exercices"
        N°27-29 p18

!!! note "Python"
    === "Types de variables"
        En python, les variables peuvent être :  
        - Des entiers : int  
        - Des réels : float  
        - Des chaines de caractères : str  
        - Des booléens : bool  
        Pour chaque type, il sera possible de faire des opérations (voir Exercices)  

    === "Exercices"
        {{ IDE('exo_type') }}

!!! info "Démonstration"
    === "Propriété"
        $\dfrac{1}{3}$ n'est pas un nombre décimal
    === "Démo"
        {{multi_qcm(
["Dire que $\dfrac{1}{3}$ est un nombre décimal équivaut à dire qu'il peut s'écire sous la forme ", 
["$a$ x $10^n$", "$\dfrac{a}{10^{-n}}$","$\dfrac{a}{10^{n}}$" ], [3]],
["Cela équivaut à dire que ",
["$3a=10^n$", "$3a=10^{-n}$", "$3$ x $10^n=a$"], [1]],
["Un nombre est un multiple de 3 si et seulement si", 
["il est pair","La somme de ses chiffres est multiple de 3","Le dernier chifrre est 3" ], [2]],
["$3a$ est un multiple de 3, donc $10^n$ est également un multiple de 3. Cependant quelle est la somme des chiffres contenus dans $10^n$ ", 
["10", "3" , "1"], [3]],
["Conclure",
["$10^n$ est un multiple de 3","$10^n$ n'est pas un multiple de 3","$\dfrac{1}{3}$ n'est pas décimal"]
,[2,3]]

)}}