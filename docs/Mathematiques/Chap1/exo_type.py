
# --------- PYODIDE:env --------- #
from math import *


# --------- PYODIDE:code --------- #
# Avec des entiers
n1=2
n2=3

# Ajouter n1 et n2 
somme1 = ...
print(somme1)

# Multiplier n1 et n2

produit1 = ....
print(produit1)

# Diviser n1 par n2

quotient1 = ...

print(quotient1)
# calculer n1 puissance n2

puissance1 = ....

print(puissance1)
# Avec des réels

r1=2.5
r2=2

# Ajouter r1 et r2 
somme2 = ...

print(somme2)
# Multiplier r1 et r2

produit2 = ....

print(produit2)

# Diviser r1 par r2

quotient2 = ....

print(quotient2)

# calculer r1 puissance r2

puissance2 = ....
print(puissance2)

# Avec des chaines de caractères

c1 = "Bonjour"
c2 = "Toto"

# Ajouter c1 et c2

concat1 = ...
print(concat1)

# Modifier léfèrement le programme pour mettre un espace entre Bonjour et Toto

concat2 = ...

print(concat2)
# Multiplier c1 par 3 

repet = .....
print(repet)
